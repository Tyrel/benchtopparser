namespace BenchtopParserTests {
    public class ProgramConfig_Tests {
        public String ProgramConfigValue;

        [SetUp]
        public void Setup() {
            ProgramConfigValue = File.ReadAllText(
              Path.Combine(TestContext.CurrentContext.TestDirectory, @"TestFiles\Program Config.txt")
          );
        }

        [Test]
        public void Test_ProgramConfig() {
            BenchtopParser.ProgramConfig programConfigs = new(ProgramConfigValue);
            Assert.IsNotNull(programConfigs);

            // Check that group collecting works - 
            Assert.That(programConfigs.Programs["P1"].I["3"].Value, Is.EqualTo("Unassigned"));
            Assert.That(programConfigs.Programs["P1"].O["3"].Value, Is.EqualTo("Test Passed"));

            Assert.That(programConfigs.Programs["P1"].P["Minimum Pressure"].Value, Is.EqualTo("95.000"));
            Assert.That(programConfigs.Programs["P1"].P["Minimum Pressure"].Unit, Is.EqualTo("mbar"));
            Assert.That(programConfigs.Programs["P1"].P["Minimum Pressure"].Type, Is.EqualTo("f"));
        }

        [Test]
        public void Test_ProgramConfig_Throw_ErrorOrder() {
            Exception ex = Assert.Throws<Exception>(delegate { new BenchtopParser.ProgramConfig("EBAE006	L	1,P1"); });
            Assert.That(ex.Message, Is.EqualTo("Setting Program Number Before Start. Error In Config"));
        }

        [Test]
        public void Test_ProgramConfig_Throw_InvalidProgram() {
            Exception ex = Assert.Throws<Exception>(delegate { new BenchtopParser.ProgramConfig("broken"); });
            Assert.That(ex.Message, Is.EqualTo("Invalid Program Config, please provide an I28 Program Config file data"));
        }

        [Test]
        public void Test_ProgramConfig_AddToGroup() {
            // Adds the "namename" config to X group
            BenchtopParser.Configuration config = new("namename", "b", "c", "d");
            String group = "x"; //testing force upper check too.
            BenchtopParser.Program program = new();
            program.AddToGroup(group, config);

            BenchtopParser.Configuration check = program.X["namename"];
            Assert.That(check, Is.EqualTo(config));
        }
    }
}
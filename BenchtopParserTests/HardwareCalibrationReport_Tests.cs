namespace BenchtopParserTests {
    public class HardwareCalibrationReport_Tests {
        public String HardwareCalibrationReportValue;
        private List<string> DocumentLines;
        [SetUp]
        public void Setup() {
            HardwareCalibrationReportValue = File.ReadAllText(
                Path.Combine(TestContext.CurrentContext.TestDirectory, @"TestFiles\Hardware Calibration Report.txt")
            );
            DocumentLines = HardwareCalibrationReportValue.Split(new string[] { Environment.NewLine }, StringSplitOptions.TrimEntries).ToList();
        }

        [Test]
        public void Test_HardwareCalibrationReport_SetInstrumentInfo() {
            DocumentLines.RemoveRange(7, DocumentLines.Count-8); // trim extra not needed for test

            BenchtopParser.HardwareCalibrationReport hcr = new();
            Assert.That(hcr.Instrument.Name, Is.EqualTo("UNSET"));
            
            hcr.SetInstrumentInfo(DocumentLines);
            Assert.That(hcr.Instrument.Name, Is.EqualTo("Chassis2 Adult"));
        }
        
        [Test]
        public void Test_HardwareCalibrationReport() {
            BenchtopParser.HardwareCalibrationReport hcr = new(HardwareCalibrationReportValue);
        }

        [Test]
        public void Test_HardwareCalibrationReport_Throw_InvalidProgram() {
            Exception ex = Assert.Throws<Exception>(delegate { new BenchtopParser.HardwareCalibrationReport("broken"); });
            Assert.That(ex.Message, Is.EqualTo("Invalid Hardware Calibration Report, please provide a Hardware Calibration Report file data"));
        }

        [Test]
        public void Test_HardwareCalibrationReport_SetIndent() {
            BenchtopParser.HardwareCalibrationReport tv = new();
            tv.SetIndent("      Transducer                             Custom");
            Assert.That(tv.Indent, Is.EqualTo(45));

            tv.SetIndent("      Transducer                         Custom");
            Assert.That(tv.Indent, Is.EqualTo(41));

            tv.SetIndent("  TransducerCustom");
            Assert.That(tv.Indent, Is.EqualTo(12));
        }

        [Test]
        public void Test_HardwareCalibrationReport_SplitLine() {
            BenchtopParser.HardwareCalibrationReport tv = new();
            string[] split;

            string context = "      Transducer                             Custom";
            tv.SetIndent(context);
            split = tv.SplitLine(context);
            Assert.That(tv.Indent, Is.EqualTo(45));
            Assert.That(split[0], Is.EqualTo("Transducer"));
            Assert.That(split[1], Is.EqualTo("Custom"));

            context = "      Transducer                         Custom";
            tv.SetIndent(context);
            split = tv.SplitLine(context);
            Assert.That(tv.Indent, Is.EqualTo(41));
            Assert.That(split[0], Is.EqualTo("Transducer"));
            Assert.That(split[1], Is.EqualTo("Custom"));

            context = "  TransducerCustom";
            tv.SetIndent(context);
            split = tv.SplitLine(context);
            Assert.That(tv.Indent, Is.EqualTo(12));
            Assert.That(split[0], Is.EqualTo("Transducer"));
            Assert.That(split[1], Is.EqualTo("Custom"));
        }
    }
}
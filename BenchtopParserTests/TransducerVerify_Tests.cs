namespace BenchtopParserTests {
    public class TransducerVerify_Tests {
        public String TransducerVerifyValue;
        [SetUp]
        public void Setup() {
            TransducerVerifyValue = File.ReadAllText(
                Path.Combine(TestContext.CurrentContext.TestDirectory, @"TestFiles\Transducer Verify.txt")
            );
        }

        [Test]
        public void Test_TransducerVerify() {
            BenchtopParser.TransducerVerify tv = new(TransducerVerifyValue);
            Assert.That(tv.Transducers[1].Id, Is.EqualTo(1));
            Assert.That(tv.Transducers[1].Name, Is.EqualTo("CTS D34-442 115PSIA"));
        }

        [Test]
        public void Test_TransducerVerify_Throw_InvalidProgram() {
            Exception ex = Assert.Throws<Exception>(delegate { new BenchtopParser.TransducerVerify("broken"); });
            Assert.That(ex.Message, Is.EqualTo("Invalid Transducer Verify Report, please provide a Transducer Verify Report file data"));
        }

        [Test]
        public void Test_TransducerVerify_SetIndent() {
            BenchtopParser.TransducerVerify tv = new();
            tv.SetIndent(1, "Transducer 1               CTS D34-442 115PSIA");
            Assert.That(tv.Indent, Is.EqualTo(26));

            tv.SetIndent(1, "Transducer 1         CTS D34-442 115PSIA");
            Assert.That(tv.Indent, Is.EqualTo(20));

            tv.SetIndent(1, "Transducer 10CTS D34-442 115PSIA");
            Assert.That(tv.Indent, Is.EqualTo(11));
        }

        [Test]
        public void Test_TransducerVerify_SplitLine() {
            BenchtopParser.TransducerVerify tv = new();
            string[] split;
            split = tv.SplitLine("Instrument Pressure 1      0.000 psig");
            Assert.That(split[0], Is.EqualTo("Instrument Pressure 1"));
            Assert.That(split[1], Is.EqualTo("0.000 psig"));
        }
    }
}
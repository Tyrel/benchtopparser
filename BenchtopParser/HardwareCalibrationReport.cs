﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchtopParser {
    public class HardwareCalibrationReport
    {
        public InstrumentInfo Instrument;
        public int Indent = 45; // Default 26 from seeing files, overridden later in SetIndent for safety

        /// Split the string, and clean up whitespace, returns the Name and Value
        public string[] SplitLine(string line) {
            return new[] {
                line[..Indent].Trim(),
                line[(Indent)..]
            };
        }
        public string[] SplitLine(int indent, string line) {
            return new[] {
                line[..indent].Trim(),
                line[(indent)..]
            };
        }
        public void SetIndent(string line) {
            const int endOfCurrentTransducerKey = 10; // "Transducer".Length calculation
            
            int leadingSpaces = line.TakeWhile(c => c == ' ').Count();
            string restOfLine = line[(leadingSpaces + endOfCurrentTransducerKey)..];
            int additionalSpaces = restOfLine.TakeWhile(c => c == ' ').Count();
            Indent = leadingSpaces + endOfCurrentTransducerKey + additionalSpaces;
            string endOfLine = line[Indent..];
            Console.Write(endOfLine);
        }

        public HardwareCalibrationReport()
        {
            Instrument = new InstrumentInfo("UNSET", "DATA", "12:00:00", "12/31/1969");
        }
        public HardwareCalibrationReport(string fileText)
        {
            const string l0Separator = "===============================================================";
            const string l1Separator = "===========================================================";

            List<string> documentLines = fileText.Split(new string[] { Environment.NewLine }, StringSplitOptions.TrimEntries).ToList();
            SetInstrumentInfo(documentLines);
            if (documentLines[0] != "|| Hardware Calibration Report ||")
            {
                throw new Exception("Invalid Hardware Calibration Report, please provide a Hardware Calibration Report file data");
            }
        }

        public void SetInstrumentInfo(List<string> documentLines)
        {
            if (documentLines[0] != "|| Instrument Info ||")
            {
                throw new Exception(
                    "Invalid Hardware Calibration Report, please provide a Hardware Calibration Report file data");
            }

            string instrumentName = SplitLine(39, documentLines[2])[1];
            string serialNumber = SplitLine(39, documentLines[3])[1];
            string time = SplitLine(39, documentLines[4])[1];
            string date = SplitLine(39, documentLines[5])[1];
            Instrument = new(instrumentName, serialNumber, time, date);
            for (int i = 0; i <= 7; i++)
            {
                documentLines.RemoveAt(0);
            }
        }
    }

    public struct InstrumentInfo
    {
        public InstrumentInfo(string name, string serialNumber, string time, string date)
        {
            Name = name;
            SerialNumber = serialNumber;
            Time = Convert.ToDateTime($"{date} {time}");
        }

        public string Name { get; }
        public string SerialNumber { get; }
        public DateTime Time { get;  }
    }
}

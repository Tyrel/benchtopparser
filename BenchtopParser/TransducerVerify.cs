﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchtopParser {
    public class TransducerVerify {

        public Dictionary<int, Transducer> Transducers = new();
        public int Indent = 26; // Default 26 from seeing files, overridden later in SetIndent for safety

        /// Split the string, and clean up whitespace, returns the Name and Value
        public string[] SplitLine(string line) {
            return new[] {
                line[..Indent].TrimEnd(),
                line[(Indent + 1)..]
            };
        }
        public void SetIndent(int id, string line) {
            int endOfCurrentTransducerKey = $"Transducer {id}".Length;
            string restOfLine = line[endOfCurrentTransducerKey..];
            int additionalSpaces = restOfLine.TakeWhile(c => c == ' ').Count();
            // length 1 based, remove 1, then add additional spaces
            Indent = endOfCurrentTransducerKey - 1 + additionalSpaces;
        }

        public TransducerVerify() {
        }

        public TransducerVerify(string fileText) {
            const string separator = "===============================================================";
            List<string> documentLines = fileText.Split(new string[] { Environment.NewLine }, StringSplitOptions.TrimEntries).ToList();

            // Check if proper document
            string title = documentLines[0];
            documentLines.RemoveAt(0);
            if (title != "|| Transducer Verify Report ||") {
                throw new Exception("Invalid Transducer Verify Report, please provide a Transducer Verify Report file data");
            }

            Dictionary<string, int> countDict = documentLines.GroupBy(p => p).ToDictionary(p => p.Key, q => q.Count());
            int currentTransducer = 0; // ONE BASED - will add 1 later at check step, but first transducer is 1.

            // Find the the indexes of each transducer start and stop
            foreach (var line in documentLines)
            {
                if (line == $"TRANSDUCER{currentTransducer + 1}") {
                    currentTransducer++;
                    continue; // Skip TITLE line and separator
                }
                if (line is separator or "") {
                    // end of current transducer
                    continue;
                }
                // Add new Transducer if not present
                if (!Transducers.ContainsKey(currentTransducer)) {
                    Transducers.Add(currentTransducer, new Transducer(currentTransducer));
                    // Calculating indent
                    SetIndent(currentTransducer, line);
                    Transducers[currentTransducer].Name = SplitLine(line)[1];

                    continue;
                }
                // Add transducer parameters
                string[] lineParts = SplitLine(line);
                Transducers[currentTransducer].Parameters[lineParts[0]] = lineParts[1];
            }
        }

    }

    public class Transducer {
        public int Id;
        public string? Name;
        public Dictionary<string, string> Parameters = new();
        public Transducer(int id) {
            Id = id;
        }
    }
}

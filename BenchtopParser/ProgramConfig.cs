﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenchtopParser {
    public class Program {
        public string? ProgramNumber { get; set; }

        // Config Groups. TODO: rename to w/e the heck they are.
        public Dictionary<string, Configuration> A = new();
        public Dictionary<string, Configuration> I = new();
        public Dictionary<string, Configuration> M = new();
        public Dictionary<string, Configuration> O = new();
        public Dictionary<string, Configuration> P = new();
        public Dictionary<string, Configuration> R = new();
        public Dictionary<string, Configuration> S = new();
        public Dictionary<string, Configuration> T = new();
        public Dictionary<string, Configuration> U = new();
        public Dictionary<string, Configuration> X = new();

        public void AddToGroup(String group, Configuration config) {
            // UGLY - but helpful later.
            // TODO: Find out what the config names are and MAKE A MAP
            string name = config.Name;
            if (name == null) {
                throw new Exception("Name is null, adding before Config has set name");
            }

            switch (group.ToUpper()) {
                case "A": this.A.Add(name, config); break;
                case "I": this.I.Add(name, config); break;
                case "M": this.M.Add(name, config); break;
                case "O": this.O.Add(name, config); break;
                case "P": this.P.Add(name, config); break;
                case "R": this.R.Add(name, config); break;
                case "S": this.S.Add(name, config); break;
                case "T": this.T.Add(name, config); break;
                case "U": this.U.Add(name, config); break;
                case "X": this.X.Add(name, config); break;
                default: throw new Exception($"No group {group} configured. Please fix configuration");
            }
        }
    }

    public class Configuration {
        public string Name { get; set; }
        public string? Value { get; set; }
        public string? Unit { get; set; }

        public string? Type { get; set; } //possible values are: a, c, f, i. Which I presume are  Array, Characters, Float, Integer

        public Configuration(string name, string? value, string? unit, string? type) {
            Name = name;
            Value = value;
            Unit = unit;
            Type = type;
        }

        public Configuration(List<string> config, string data) {
            string[] nameValue = data.Split("=");
            Name = nameValue[0].Trim();
            Value = nameValue[1].Trim();

            Unit = config[1].Trim();
            Type = config[2].Trim();
        }
    }

    public class ProgramConfig {
        public Dictionary<String, Program> Programs = new();
        public ProgramConfig(string fileText) {
            Program? current = null;

            foreach (string line in fileText.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)) {
                List<string> configLine = line.Split("\t").ToList();
                if (configLine.Count < 2) {
                    throw new Exception("Invalid Program Config, please provide an I28 Program Config file data");
                }

                // Remove the first two items because they seem to be a checksum and an unneeded type.
                // Then we can start at 0 like normal, not have the first important bit be at 2.
                configLine.RemoveAt(0); // Remove random hex value
                configLine.RemoveAt(0); // Remove L column

                /* LOOP CONTROL */
                if (configLine[0].StartsWith("Start,")) {
                    // initialize programconfig
                    current = new Program();
                    continue;
                }
                if (configLine[0].StartsWith("Stop,")) {
                    // Short circuit early when stop comes.
                    if (current?.ProgramNumber != null) {
                        Programs.Add(current.ProgramNumber, current);
                    }
                    current = null;
                    // TODO: What if multiple configs how to handle start and stop?
                    break;
                }
                List<string> config = configLine[0].Split(",", 2).ToList();
                if (configLine.Count == 1) {
                    // Set Program Number
                    if (current == null) {
                        throw new Exception("Setting Program Number Before Start. Error In Config");
                    }
                    current.ProgramNumber = config[1];
                    continue;
                }
                /* CREATE A CONFIGURATION */

                string value = config[1];

                string[] programnumberGroupData = value.Split('\\'); // snake case as its three values
                string data = programnumberGroupData[2];
                if (data.StartsWith("=")) { continue; /* comment line */ }

                Configuration lineConfig = new(configLine, data);

                // Deal with this group logic
                current?.AddToGroup(programnumberGroupData[1], lineConfig);
            }
            // TODO: What if multiple configs how to handle start and stop?
        }

    }

}
